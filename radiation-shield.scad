// clang-format off
include <utils/utils.scad>;
use <threads-scad/threads.scad>;
// clang-format on

/* [👁 Display] */
debug = false;
// render some operations to get rid of glitches
render_some = false;

/* [🛡 Radiation Shield] */
flat_diameter = 100;   // [50:0.1:200]
flat_length = 0;       // [0:0.1:200]
flat_rotation = 0;     // [-90:0.1:90]
thickness = 2.5;       // [1:0.1:10]
edge_type = "⚫ round"; // ["⚫ round","🔺 chamfer"]
edge_size = 10;        // [0:0.1:30]
edge_height = 20;      // [0:0.1:100]
rim_height = 0;        // [0:0.1:20]

/* [🍽 PCB] */
show_pcb = false;
// clang-format off

pcb_file = "sensor-boom-drl_map.svg"; // ["calPlate-drl_map.svg","sensor-boom-drl_map.svg"]

// clang-format on

pcb_offset = [ 0, 0, 0 ]; // [-100:0.01:100]

/* [⚫ Fixture] */
fixture_type = "⭕ hole"; // ["⭕ hole","⚫ pin","🔘 hole pin"]
fixture_diameter = 6;
fixture_length = 6;
fixture_inner_diameter = 2.8;
fixture_offset = [ 0, 0 ]; // [-100:0.1:100]
// specify as list [X1,Y1,X2,Y2,...]
fixture_positions = []; // [-100:0.01:100]
// reshape afterwards due to #3817 (Customizer not able to restore nested
// vectors)
fixture_positions_ = groupn(fixture_positions, 2);

/* [💥 Boom] */
with_boom = true;
boom_width = 30;                   // [5:0.1:50]
boom_thickness = 2.5;              // [1:0.1:5]
boom_length = 100;                 // [0:0.1:300]
boom_plug_length = 30;             // [5:0.1:100]
boom_plug_narrow = 5;              // [0:0.1:20]
boom_plug_wide = 20;               // [0:0.1:50]
boom_plug_thickness = 10;          // [1:0.1:20]
boom_plug_transition_length = -10; // [-50:0.1:100]
boom_plug_rotation = [ 0, 0, 0 ];  //  [-90:0.1:90]
boom_plug_offset = [ 0, 0, 0 ];    // [-50:0.1:50]
boom_plug_thread = true;
// [pitch, tooth angle, tolerance]
boom_plug_thread_params = [ 0, 30, 0.4 ]; // [0:0.01:100]
boom_plug_thread_depth = 5;               // [0:0.01:100]
boom_plug_thread_diameter = 10;           // [0:0.01:100]
boom_plug_thread_offset = [ 0, 0, 0 ];    // [-100:0.01:100]
boom_transition_length = 20;              // [0:0.1:100]
boom_support = true;
boom_support_thickness = 2; // [0.1:0.1:10]

/* [🔌 Cable] */
cable_hole = false;
cable_hole_debug = false;
cable_hole_position_angle = 0; // [-180:0.1:180]
cable_hole_offset = [ 0, 0 ];  // [-50:0.1:50]
cable_hole_size = [ 16, 14 ];
cable_hole_edge_radius = 0; // [0:0.1:40]

cable_hole_boom = false;
cable_hole_boom_position = [ 0, 0 ]; // [-50:0.1:50]
cable_hole_boom_size = [ 16, 14 ];
cable_hole_boom_edge_radius = 0; // [0:0.1:40]

// Precision
epsilon = 0.1 * 1;
$fa = $preview ? 3 : 2;
$fs = $preview ? 2 : 0.5;

module
edge_shape()
{
  resize([ 2 * (edge_size - thickness),
           2 * (edge_height - thickness) ]) if (edge_type == "⚫ round")
    circle(max(edge_size, edge_height) - thickness);
  else if (edge_type == "🔺 chamfer") rotate([ 0, 0, 45 ])
    square([ 1, 1 ], center = true);
}

module disk_shape(flat = true, edge = true, rim = true, extra_offset = 0)
{
  offset(extra_offset)
  {
    // the flat part
    if (flat)
      square([ flat_diameter / 2, thickness ]);
    // the edge
    if (edge)
      translate([ flat_diameter / 2, 0 ]) intersection()
      {
        translate([ 0, edge_height ])
        {
          difference()
          {
            offset(delta = thickness, chamfer = true) edge_shape();
            edge_shape();
          }
        }
        square([ edge_size, edge_height ]);
      }
    // the extra rim
    if (rim)
      translate([ flat_diameter / 2 + edge_size - thickness, edge_height ])
        square([ thickness, rim_height ]);
  }
}

module shield(flat = true, edge = true, rim = true, extra_offset = 0)
{
  rotate([ 0, 0, 180 ]) rotate_extrude(angle = 180) disk_shape(
    flat = flat, edge = edge, rim = rim, extra_offset = extra_offset);
  translate([ 0, flat_length, 0 ]) rotate([ 90, 0, 0 ]) render(convexity = 2)
    linear_extrude(flat_length)
  {
    disk_shape(
      flat = flat, edge = edge, rim = rim, extra_offset = extra_offset);
    mirror([ 1, 0, 0 ]) disk_shape(
      flat = flat, edge = edge, rim = rim, extra_offset = extra_offset);
  };
  translate([ 0, flat_length, 0 ]) rotate_extrude(angle = 180) disk_shape(
    flat = flat, edge = edge, rim = rim, extra_offset = extra_offset);
}

module
cable_hole_cutout()
{
  intersection()
  {
    shield(flat = false, extra_offset = 2);
    hole_length =
      (flat_diameter / 2 + flat_length + thickness + edge_size + epsilon);
    highlight_if(cable_hole_debug) rotate([ 0, 0, cable_hole_position_angle ])
      translate([ 0, 0, thickness / 2 + edge_height / 2 ]) rotate([ 90, 0, 0 ])
        linear_extrude(hole_length) translate(cable_hole_offset)
          offset(cable_hole_edge_radius) offset(-cable_hole_edge_radius)
            square(cable_hole_size, center = true);
  }
}

rotate(flat_rotation) render_if(render_some) difference()
{
  // the disk shape
  union()
  {
    shield();
    if (fixture_type == "⚫ pin" || fixture_type == "🔘 hole pin") {
      for (pos = fixture_positions_) {
        translate(pos) translate(fixture_offset) translate([ 0, 0, thickness ])
          difference()
        {
          cylinder(d = fixture_diameter, h = fixture_length);
          if (fixture_type == "🔘 hole pin") {
            cylinder(d = fixture_inner_diameter, h = fixture_length + epsilon);
          }
        }
      }
    }
  }
  // fixture holes
  if (fixture_type == "⭕ hole") {
    for (pos = fixture_positions_) {
      translate(pos) translate(fixture_offset) translate([ 0, 0, -epsilon / 2 ])
        cylinder(d = fixture_diameter, h = thickness + epsilon);
    }
  }
  // cable hole
  if (cable_hole) {
    cable_hole_cutout();
  }
}

if (show_pcb)
  rotate([ 0, 0, flat_rotation ]) translate(pcb_offset)
    translate(fixture_offset) translate([ 0, 0, thickness ]) color("black", 0.5)
      linear_extrude(1.5) import(pcb_file);

module
plug_shape()
{
  polygon([
    [ -boom_plug_narrow / 2, 0 ],
    [ -boom_plug_wide / 2, boom_plug_thickness ],
    [ boom_plug_wide / 2, boom_plug_thickness ],
    [ boom_plug_narrow / 2, 0 ],
  ]);
}

module
plug()
{
  linear_extrude(boom_plug_length) plug_shape();
}

module
move_to_plug_position()
{
  rotate([ 0, 0, 180 ]) translate(boom_plug_offset) translate([
    0,
    boom_length + flat_diameter / 2 - boom_plug_thickness,
    boom_thickness
  ]) rotate(boom_plug_rotation) children();
}

if (with_boom) {
  move_to_plug_position()
  {
    if (boom_plug_thread)
      render_if(render_some) ScrewHole(
        boom_plug_thread_diameter,
        boom_plug_thread_depth,
        position = add([ 0, boom_plug_thickness, boom_plug_length / 2 ],
                       boom_plug_thread_offset),
        rotation = [ 90, 0, 0 ],
        pitch = boom_plug_thread_params[0],
        tooth_angle = boom_plug_thread_params[1],
        tolerance = boom_plug_thread_params[2]) plug();
    else
      plug();
  }

  rotate([ 0, 0, 180 ])
  {
    // transition from plug to boom
    linear_extrude(boom_thickness) hull()
    {

      translate([ 0, flat_diameter / 2 + boom_length - boom_plug_thickness ])
      {
        plug_shape();
        translate([ 0, -boom_plug_transition_length ])
          translate([ 0, -epsilon ])
            square([ boom_width, epsilon ], center = true);
      }
    }
    // boom
    translate(
      [ -boom_width / 2, flat_diameter / 2 + boom_transition_length, 0 ])
      cube([
        boom_width,
        boom_length - boom_plug_transition_length - boom_transition_length -
          boom_plug_thickness,
        boom_thickness
      ]);
    // transition from boom to shield
    linear_extrude(boom_thickness) difference()
    {
      hull()
      {
        translate([ 0, flat_diameter / 2 + boom_transition_length ])
          translate([ 0, epsilon / 2 ])
            square([ boom_width, epsilon ], center = true);
        circle(d = flat_diameter);
      }
      circle(d = flat_diameter);
    }
  }

  if (boom_support) {
    render_if(render_some) difference()
    {
      union()
      {
        hull()
        {
          angle = boom_support_thickness /
                  (2 * PI * (flat_diameter / 2 + edge_size)) * 360;
          rotate([ 0, 0, 180 ]) translate([ 0, thickness / 2 ])
            rotate([ 0, 0, 90 - angle / 2 ]) rotate_extrude(angle = angle)
              disk_shape(edge = true, rim = true, flat = false);
          move_to_plug_position() translate([ -boom_support_thickness / 2, 0 ])
            cube([ boom_support_thickness, epsilon, boom_plug_length ]);
          rotate([ 0, 0, 180 ]) translate(
            [ 0, flat_diameter / 2 + boom_length - boom_plug_thickness ])
            cube(boom_thickness);
        }
        hull()
        {
          // transition from plug to boom
          rotate([ 0, 0, 180 ]) linear_extrude(boom_thickness) translate(
            [ 0, flat_diameter / 2 + boom_length - boom_plug_thickness ])
            plug_shape();
          move_to_plug_position() linear_extrude(epsilon) plug_shape();
        }
      }
      if (cable_hole_boom) {
        rotate([ 0, 0, 180 ]) translate(
          [ 0, flat_diameter / 2 + boom_length / 2, boom_plug_length / 2 ])
          rotate([ 0, 90, 0 ]) translate(cable_hole_boom_position)
            linear_extrude(
              sum(map(
                function(x) abs(x),
                flatten(
                  [ boom_thickness, 2 * boom_plug_length, boom_plug_offset ]))),
              center = true) offset(cable_hole_boom_edge_radius)
              offset(-cable_hole_boom_edge_radius)
                square(cable_hole_boom_size, center = true);
      }
    }
  }
}
